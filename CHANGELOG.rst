eole-workstation
================

2.8.1 (2021-02-24)

* Veyon: fix enabling of disabled features

2.8.0 (2021-02-09)

* Veyon: conditional control of workstations

2.7.5 (2020-11-30)

* Veyon: enable PPA for Ubuntu Focal

2.7.4 (2020-11-19)

* Salt minion: update to 3002.2

2.7.3 (2020-11-16)

* Salt minion: select proper version

2.7.2 (2020-11-16)

* Salt minion: update to 3002.1

2.7.1 (2020-10-29)

* Veyon: configure “AuthorizedUserGroups” from pillar

2.7.0 (2020-10-15)

* use “ad-formula” and “pam-mount” formula. We keep compatibility sls for now.

2.6.1 (2020-09-23)

* reboot: fix loop because of disabled windows update
* map.jinja: update to v5
* libtofs: use Black style formatting

2.6.0 (2020-08-24)

* Salt minion: update to 3001.1

2.5.2 (2020-07-22)

* Salt minion: fix removal of configuration files by “clean: True”

2.5.1 (2020-07-22)

* Salt minion: fix the configuration source

2.5.0 (2020-07-22)

* Salt minion: install latest 3001 version and manage its configuration
* Veyon: update to release 4.4.1
* AD join: remove workaround and use internal system to detect needed reboot

2.4.0 (2020-04-24)

* Disable user consent since it's not usable for us
* Force the update of the mine at the end of the formula

2.3.1 (2020-02-19)

* Fix self user content query

2.3.0 (2020-02-14)

* Deactivate actions related to computer not listed.

2.2.0 (2020-02-06)

* Use rules instead of group whitelist to implement user consent.

2.1.0 (2919-11-15)

* Update map.jinja to latest formula standards
* Fix restart of service on configuration change

2.0.0 (2919-11-14)

* Update to latest formula standards

1.2.1 (2019-11-14)

* Veyon: search LDAP objects recursively in the tree from base dn

1.2.0 (2019-11-13)

* Veyon: update to release 4.3.0

1.1.4 (2019-09-06)

- AD join: Avoid failing states when the computer is already joined

1.1.3 (2019-07-15)

- Import Veyon configuration on file changes or LDAP connexion errors

1.1.2 (2019-03-28)

- Register Veyon service when it's not enabled
- Import config and set LDAP when LDAP connection is not working
- Hide Veyon password from “state.apply” output

1.1.1 (2019-02-07)

- Configure Veyon software

1.1.0 (2019-01-31)

- Install Veyon software

1.0.0 (2019-01-23)

- Implement the Active Directory join
