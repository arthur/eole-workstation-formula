# -*- mode: yaml; coding: utf-8 -*-
## vim: ft=yaml ##

# both 32-bit (x86) AND a 64-bit (AMD64) installer available
{% if grains['cpuarch'] == 'AMD64' %}
{% set arch = 'win64' %}
{% else %}
{% set arch = 'win32' %}
{% endif %}

{% set versions = ['4.4.1.0', '4.3.0.0'] %}

veyon:
{% for version in versions %}
  '{{ version }}':
    full_name: 'Veyon'
    installer: 'http://salt/joineole/veyon/veyon-{{ version }}-{{ arch }}-setup.exe'
    install_flags: '/S /Master'
    uninstaller: '%ProgramFiles%\Veyon\uninstall.exe'
    uninstall_flags: '/S'
    msiexec: False
    locale: en_US
    reboot: False
{% endfor %}
