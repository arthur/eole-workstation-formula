# -*- mode: yaml; coding: utf-8 -*-
## vim: ft=yaml ##

{%- set arch = grains['cpuarch'] %}
{%- set versions = ['3002.2', '3002.1', '3001.1', '3001', '2018.3.3'] %}

salt-minion:
{%- for version in versions %}
  '{{ version }}':
    full_name: 'Salt Minion {{ version }} (Python 3)'
    installer: 'http://salt/joineole/saltstack/Salt-Minion-{{ version }}-Py3-{{ arch }}-Setup.exe'
    install_flags: '/S'
    uninstaller: '%SystemDrive%\salt\uninst.exe'
    uninstall_flags: '/S'
    msiexec: False
    use_scheduler: True
    reboot: False
{%- endfor %}
