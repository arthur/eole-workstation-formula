.. _readme:

========================
eole-workstation-formula
========================

Formula to setup and configure EOLE workstations.


.. contents:: **Table of Contents**

General notes
-------------

See the full `SaltStack Formulas installation and usage instructions
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

If you are interested in writing or contributing to formulas, please pay attention to the `Writing Formula Section
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#writing-formulas>`_.

If you want to use this formula, please pay attention to the ``FORMULA`` file and/or ``git tag``,
which contains the currently released version. This formula is versioned according to `Semantic Versioning <http://semver.org/>`_.

See `Formula Versioning Section <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#versioning>`_ for more details.

Available states
----------------

.. contents::
    :local:

``eole-workstation``
^^^^^^^^^^^^^^^^^^^^

Workstations management entry point. It includes all the others SLS.

``eole-workstation.ad``
^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

**Deprecated** use ``salt '*' state.apply ad.join``.

Manage workstations in an Active Directory. It include all SLS under
the namespace `eole-workstation.ad`.

``eole-workstation.ad.join``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

**Deprecated** use ``salt '*' state.apply ad.join``.

Add  ``ad/member`` to ``roles`` grain.

It depends on:

- ``ad.join``

``eole-workstation.ad.leave``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

**Deprecated** use ``salt '*' state.apply ad.leave``.

Remove ``ad/member`` from ``roles`` grain.

It depends on:

- ``ad.join``

``eole-workstation.salt.minion``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

Install the salt minion package, configure the service and make sure
the service is enabled and running.

``eole-workstation.salt.minion.package``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will install the salt-minion package only.

``eole-workstation.salt.minion.config``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the salt-minion service and has a dependency
on ``eole-workstation.salt.minion.package``

``eole-workstation.salt.minion.service``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will enable and start the salt-minion service and react to
any change to the configuration to restart the service.

``eole-workstation.veyon``
^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

Install the Veyon_ software, configure the service and make sure the
service is enabled and running.

``eole-workstation.veyon.repo``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

Configure an additionnal repository for Veyon.

``eole-workstation.veyon.repo.install``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Configure an additionnal repository for Veyon if ``eole-workstation:veyon:pkg:use_upstream_repo`` is ``True`` and ``eole-workstation:veyon:pkg:repo``  is defined.

``eole-workstation.veyon.package``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will install the Veyon package only.

It depends on ``eole-workstation.veyon.repo.install`` if  ``eole-workstation:veyon:pkg:use_upstream_repo`` is ``True``.

``eole-workstation.veyon.config``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the Veyon service and has a dependency on
``eole-workstation.veyon.package`` via include list.

See `pillar.example` for required configuration options.

``eole-workstation.veyon.service``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will enable and start the Veyon service and has a
dependency on ``eole-workstation.veyon.config`` via include list.

``eole-workstation.veyon.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will undo everything performed in the
``eole-workstation.veyon`` meta-state in reverse order, i.e.:

- stop and disable the service
- purge the configuration
- remove the package

``eole-workstation.veyon.service.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will stop and unregister the Veyon service.

``eole-workstation.veyon.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will clear the service configuration from registry, remove
the configuration file from the disk and has a dependency on
``eole-workstation.veyon.service.clean`` via include list.

``eole-workstation.veyon.package.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the Veyon package and has a dependency on
``eole-workstation.veyon.config.clean`` via include list.

It depends on ``eole-workstation.veyon.repo.clean`` if  ``eole-workstation:veyon:pkg:use_upstream_repo`` is ``True``.

``eole-workstation.veyon.repo.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Remove the Veyon additional repository.

``eole-workstation.mine``
^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state manage the minion mine. It actually only force the update
of the mine.

``eole-workstation.mine.update``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state force the update of the mine.


Available softwares
-------------------

.. contents::
   :local:

We provide `winrepo-ng` SLS to permit the installation of softwares on
Microsoft Windows machines.

``veyon``
^^^^^^^^^

Veyon_ is a free and Open Source software for computer monitoring and
classroom management supporting Linux and Windows.

.. _Veyon: https://veyon.io/
