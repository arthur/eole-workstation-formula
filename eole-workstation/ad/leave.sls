# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_ad_leave = "ad.leave" %}

include:
  - {{ sls_ad_leave }}

eole-workstation/ad/leave/grains.list_absent:
  grains.list_absent:
    - name: roles
    - value:
        - ad/member
    - require_in:
        - sls: {{ sls_ad_leave }}
