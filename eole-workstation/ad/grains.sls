# -*- mode: salt; coding: utf-8 -*-
# vim ft=sls

{% set sls_join = "ad.member." ~ grains["kernel"] | lower ~ ".join" %}

include:
  - {{ sls_join }}

eole-workstation/ad/grains/grains.list_append:
  grains.list_present:
    - name: roles
    - value:
        - ad/member
    - require_in:
        - sls: {{ sls_join }}
