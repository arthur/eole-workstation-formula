# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{%- set sls_ad_join = "ad.join" %}

include:
  - {{ sls_ad_join }}

eole-workstation/ad/grains/grains.list_append:
  grains.list_present:
    - name: roles
    - value:
        - ad/member
    - require_in:
        - sls: {{ sls_ad_join }}
