# -*- mode: salt; coding: utf-8 -*-
# vim ft=sls

{% set sls_leave = "ad.leave" %}

include:
  - {{ sls_leave }}

eole-workstation/ad/grains/grains.list_append:
  grains.list_absent:
    - name: roles
    - value:
        - ad/member
