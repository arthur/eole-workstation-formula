# -*- mode: salt; coding: utf-8 -*-

# Manually reboot since we can't set a timeout on system.join_domain
Reboot after joining the domain:
  system.reboot:
    - message: The system requires to be rebooted
    - timeout: 5
    - in_seconds: True
    - only_on_pending_reboot: True
