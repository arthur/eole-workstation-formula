# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.salt.minion.package.install' %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}
{%- from tplroot ~ '/libtofs.jinja' import files_switch with context %}

{%- set saltstack = ews.salt %}

{%- if grains['os_family'] == 'Windows' %}
{%-   set system_drive = salt['environ.get']('SYSTEMDRIVE', 'C:') %}
{%-   set config_dir = system_drive ~ saltstack.minion.config %}
{%- else %}
{%-   set config_dir = saltstack.minion.config %}
{%- endif %}

include:
  - {{ sls_package_install }}

eole-workstation/salt/minion/config/file/file-recurse:
  file.recurse:
    - name: {{ config_dir }}
    - source: {{ files_switch(['minion.d'],
                              lookup='eole-workstation/salt/minion/config/file/file-recurse',
                              use_subpath=True
                 )
              }}
    - clean: True
    - exclude_pat: _*
    - template: jinja
    - context:
        config: {{ saltstack | json }}
    - require:
      - sls: {{ sls_package_install }}
