# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

{%- set saltstack = ews.salt %}

include:
  - {{ sls_package_install }}

eole-workstation/salt/minion/config/clean/file-tidied:
  file.tidied:
    - name: {{ saltstack.minion.config }}
    - matches:
        - '[^_].*'
    - rmdirs: True
