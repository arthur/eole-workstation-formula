# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}
{%- set sls_config_file = tplroot ~ '.salt.minion.config.dir' %}

{%- set saltstack = ews.salt %}

include:
  - {{ sls_config_file }}

eole-workstation/salt/minion/service/running/service-running:
  service.running:
    - name: {{ saltstack.minion.service.name }}
    - enable: True
    - watch:
      - sls: {{ sls_config_file }}
