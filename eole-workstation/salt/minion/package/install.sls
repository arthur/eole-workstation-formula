# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

{%- set saltstack = ews.salt %}

eole-workstation/salt/minion/package/install/pkg.installed:
  pkg.installed:
    - name: {{ saltstack.minion.pkg.name }}
    - version: {{ saltstack.version }}
