# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_veyon_service_clean = tplroot ~ '.veyon.service.clean' %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

include:
  - {{ sls_veyon_service_clean }}

{%- set veyon = ews.veyon %}


# Always use "cwd" argument
# This avoid error with space in "Program Files"
Clear Veyon configuration:
  cmd.run:
    - name: {{ veyon.veyon_cli }} config clear
    - cwd: {{ veyon.config_dir }}
    - require:
      - sls: {{ sls_veyon_service_clean }}


Remove configuration file:
  file.absent:
    - name: {{ veyon.config_dir | path_join(veyon.config_file) }}
