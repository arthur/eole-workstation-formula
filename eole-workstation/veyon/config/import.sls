# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_veyon_config_file = '.file' %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

include:
  - {{ sls_veyon_config_file }}

{%- set veyon = ews.veyon %}

{%- if grains["os_family"] == "Windows" %}
{%-   set veyon_password_var = "%VEYON_PASSWORD%" %}
{%- else %}
{%-   set veyon_password_var = "$VEYON_PASSWORD" %}
{%- endif %}

# Always use "cwd" argument
# This avoid error with space in "Program Files"
Import Veyon configuration:
  cmd.run:
    - name: {{ veyon.veyon_cli }} config import {{ veyon.config_file }}
    - cwd: {{ veyon.config_dir }}
    - onchanges:
      - file: Download Veyon Configuration


# Always use "cwd" argument
# This avoid error with space in "Program Files"
# Hide password by passing it as environment variable
Set LDAP password:
  cmd.run:
    - name: {{ veyon.veyon_cli }} config set LDAP/BindPassword {{ veyon_password_var }}
    - cwd: {{ veyon.config_dir }}
    - env:
      - VEYON_PASSWORD: {{ veyon.ldap.password }}
    - require:
      - cmd: Import Veyon configuration
    - unless:
      - cd {{ veyon.config_dir }} && {{ veyon.check_ldap }}
