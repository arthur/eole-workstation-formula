# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_veyon_package_install = tplroot ~ '.veyon.package.install' %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}
{%- from tplroot ~ '/libtofs.jinja' import files_switch with context %}

include:
  - {{ sls_veyon_package_install }}

{%- set veyon = ews.veyon %}

# This will force an import if the LDAP connexion is not working
Cleanup configuration file on error:
  file.absent:
    - name: {{ veyon.config_dir | path_join(veyon.config_file) }}
    - unless:
      - cd {{ veyon.config_dir }} && {{ veyon.check_ldap }}


Download Veyon Configuration:
  file.managed:
    - name: {{ veyon.config_dir | path_join(veyon.config_file) }}
    - source: {{ files_switch(['veyon-config.json'],
                              lookup='Download Veyon Configuration',
                              use_subpath=True
      ) }}
    - template: jinja
    - context:
        veyon: {{ veyon | json }}
    - require:
      - sls: {{ sls_veyon_package_install }}
