# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as ews with context %}
{%- from tplroot ~ "/libkwargs.jinja" import format_kwargs with context %}

eole-workstation/veyon/repo/clean/pkgrepo.managed:
  pkgrepo.absent:
    {{- format_kwargs(ews.veyon.pkg.repo) }}
    - onlyif: {{ ews.veyon.pkg.repo and ews.veyon.pkg.use_upstream_repo }}
