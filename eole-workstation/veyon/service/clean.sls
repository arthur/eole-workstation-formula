# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

{%- set veyon = ews.veyon %}


# Use cmd.run to bypass service module bug on windows
# Always use "cwd" argument
# This avoid error with space in "Program Files"
Stop Veyon service:
  cmd.run:
    - name: {{ veyon.veyon_cli }} service stop
    - cwd: {{ veyon.config_dir }}


# Use cmd.run to bypass service module bug on windows
# Always use "cwd" argument
# This avoid error with space in "Program Files"
Register Veyon service:
  cmd.run:
    - name: {{ veyon.veyon_cli }} service unregister
    - cwd: {{ veyon.config_dir }}
    - require:
      - cmd: Stop Veyon service
