# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_veyon_config_import = tplroot ~ '.veyon.config.import' %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

include:
  - {{ sls_veyon_config_import }}

{%- set veyon = ews.veyon %}


Start Veyon service:
  service.running:
    - name: {{ veyon.service }}
    - enable: True
    - require:
      - sls: {{ sls_veyon_config_import }}


# Use cmd.run to bypass service module bug on windows (sometimes)
# Always use "cwd" argument
# This avoid error with space in "Program Files"
Register Veyon service:
  cmd.run:
    - name: {{ veyon.veyon_cli }} service register
    - cwd: {{ veyon.config_dir }}
    - onfail:
      - service: Start Veyon service

# Workaround error on windows service restart
# Always use "cwd" argument
# This avoid error with space in "Program Files"
Restart Veyon Service:
  cmd.run:
    - name: {{ veyon.veyon_cli }} service restart
    - cwd: {{ veyon.config_dir }}
    - onchanges:
      - cmd: Import Veyon configuration
      - cmd: Set LDAP password
