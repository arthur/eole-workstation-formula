# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

{%- set sls_veyon_config_clean = tplroot ~ '.veyon.config.clean' %}
{%- set sls_veyon_repo_clean = tplroot ~ ".veyon.repo.clean" %}

{%- set requires = [{"sls": sls_veyon_config_clean}] %}
{%- if ews.veyon.pkg.use_upstream_repo %}
{%-   do requires.append({'sls': sls_veyon_repo_clean}) %}
{%- endif %}

{#- serialize as YAML to avoid encoding errors on Python2 #}
include: {{ requires | map(attribute="sls") | list | yaml }}

Remove Veyon grains:
  grains.list_absent:
    - name: roles
    - value:
        - veyon/master
        - veyon/client

Uninstall Veyon software:
  pkg.removed:
    - name: veyon
    - require: {{ requires | yaml }}
