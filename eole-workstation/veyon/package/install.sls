# -*- mode: salt; coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ '/map.jinja' import mapdata as ews with context %}

{%- set veyon = ews.veyon %}

{%- set sls_repo_install = tplroot ~ ".veyon.repo.install" %}

{%- if veyon.pkg.use_upstream_repo %}
{%-   set requires = [{"sls": sls_repo_install}] %}
{%- else %}
{%-   set requires = [] %}
{%- endif %}

{#- serialize as YAML to avoid encoding errors on Python2 #}
include: {{ requires | map(attribute="sls") | list | yaml }}

Add Veyon grains:
  grains.list_present:
    - name: roles
    - value:
        - veyon/master
        - veyon/client

Install Veyon software:
  pkg.installed:
    - name: {{ veyon.pkg.name }}
    - require: {{ requires | yaml }}
